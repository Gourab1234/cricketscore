package espn.cricinfo;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
public class LiveScore extends MatchIntializer {
	protected List<String> url = new ArrayList<String>();
	protected ArrayList<WebElement> matches = (ArrayList<WebElement>) driver
			.findElements(By.xpath("//div[@class='match-score-block']/div[2]/a"));

	public void getMatchLinks() {
		// System.out.println(matches.size());
		for (int i = 1; i <= matches.size(); i++) {
			String match = ((matches.get(i - 1).getAttribute("href")));
			url.add(match);
		}
	}
	public void clickMatchLinks() {
		for (int i = 0; i < url.size(); i++) {
			//System.out.print("Link: " + url.get(i));
			driver.navigate().to(url.get(i));
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			//System.out.print("  Link Page Title: ");
			//System.out.println(driver.getTitle());
}
}
}