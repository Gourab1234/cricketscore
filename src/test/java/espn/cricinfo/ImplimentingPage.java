package espn.cricinfo;

public class ImplimentingPage {
	public static void main(String[] args) {
		MatchIntializer in = new MatchIntializer();
		in.initDriver();
		in.navigateEspncricinfo();
		in.maximizeWindow();
		in.waitToLoad();
		CricketInfo hm = new CricketInfo();
		hm.clickOnElements();
		LiveScore clp = new LiveScore();
		clp.getMatchLinks();
		clp.clickMatchLinks();
}
}