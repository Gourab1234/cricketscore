package espn.cricinfo;

import java.util.concurrent.TimeUnit;

public class MatchIntializer {
	static WebDriver driver;

	public void initDriver() {
		System.setProperty("webdriver.chrome.driver","D:\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void navigateEspncricinfo() {
		driver.navigate().to("https://www.espncricinfo.com");
	}

	public void maximizeWindow() {
		driver.manage().window().maximize();
	}

	public void waitToLoad() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
}
}